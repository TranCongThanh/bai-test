var canvas = document.querySelector('.container');
var context = canvas.getContext('2d');
var valueSquare = 20; // đề bài cho là 20 ô. thì e cho width height = 400, => độ dài rộng của mỗi phần tử là 20x20
var food;
var score = 0;
var snakeArr // mảng chứa các phần tử snake
var keyCodeValue; // keycode từ bàn phím
var mode  = 1; // check mode. khỏi tạo mode ban đầu là mode 1
// khỏi tạo 

function init() {
    keyCodeValue = 'right'// khởi tạo hướng ban đầu là right
    createSnake()
    createFood()
    score = 0;
    if(typeof gameLoop != "undefined") {
        clearInterval(gameLoop); 
    }
    gameLoop = setInterval(drawFoodAndSnake,300); // đề bài là 1s 1 ô thì ta set 1000 thì nó sẽ lập mỗi giây 1 ô, mà con rắn nó sẽ chậm, e chỉnh nhanh hơn xíu để test
} 
init();
//tạo rắn
function createSnake(){
    snakeArr = [{x:10,y:10}]
}
//tạo đồ ăn
function createFood() {
    food = {
        x: Math.round(Math.random() * 350/20),
        y: Math.round(Math.random() * 350/20)
    }
}
function paintCell(x, y){
    context.fillStyle = "red";
    context.fillRect(x*20, y*20, 20, 20);
    context.strokeStyle = "white";
    context.strokeRect(x*20, y*20, 20, 20);
}
    
function checkCollision(x, y, array) {
    for(var i = 0; i < array.length; i++)
    {
        if(array[i].x == x && array[i].y == y)
            return true;
    }
    return false;
}
var mode = 1;
function changeMode2(){
    mode = 2;
    console.log('hello')
}
// vẽ thông qua cancas HTML5
function drawFoodAndSnake () {
    context.fillStyle = "white";
    context.fillRect(0, 0, 400, 400);

    var snakeX = snakeArr[0].x
    var snakeY = snakeArr[0].y
    if(keyCodeValue == 'right'){
        snakeX++
    } else if(keyCodeValue == 'left'){
        snakeX--
    } else if(keyCodeValue == 'up'){
        snakeY--
    } else if (keyCodeValue == 'down'){
        snakeY++
    }
 // mode chạm tường

if(mode ==1 ){
    if(snakeX==-1|| snakeX ==400/20|| snakeY == -1 || snakeY == 400/20||checkCollision(snakeX, snakeY, snakeArr) ) {
        init();
        return;
    }
} else if (mode == 2){
    if(snakeX < 0) {
        snakeX = 400/20 
    } else if(snakeX >= 400/20) {
        snakeX = 0
    } else if(snakeY < 0) {
        snakeY = 400/20
    } else if(snakeY >= 400/20){
        snakeY = 0
    }
}
 // xét sự va chạm với đồ ăn: tăng điểm + tạo đồ ăn mới   
	if(snakeX == food.x && snakeY == food.y) {
        var tail = {x: snakeX, y: snakeY};
        score++;
        createFood();
    } else {
        var tail = snakeArr.pop(); // xóa cuối mảng để con rắn nó kéo nguyên dám đi được
        tail.x = snakeX; 
        tail.y = snakeY;
    }
        snakeArr.unshift(tail); // lấy đít gắn vào đầu để di chuyển
        for(var i = 0; i < snakeArr.length; i++){
        var c = snakeArr[i];
        paintCell(c.x, c.y); // vẽ rắn
    }
        paintCell(food.x, food.y); // vẽ đồ ăn
        var score_text = "Score: " + score;
        context.fillText(score_text, 400/2, 400/2);
}
window.addEventListener('keydown', function(e) {
    if (e.keyCode === 37) {
        keyCodeValue = 'left'
        console.log(keyCodeValue)
    } else if (e.keyCode === 38) {
        keyCodeValue = 'up'
        console.log(keyCodeValue)
    } else if (e.keyCode === 39) {
        keyCodeValue = 'right'
        console.log(keyCodeValue)
    } else if (e.keyCode === 40) {
        keyCodeValue = 'down'
        console.log(keyCodeValue)
    }
    });